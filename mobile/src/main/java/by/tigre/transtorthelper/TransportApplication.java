package by.tigre.transtorthelper;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.raizlabs.android.dbflow.config.FlowManager;

import by.tigre.transtorthelper.core.data.TransportDatabase;

/**
 * Created by igor on 3/28/15.
 */
public class TransportApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
        TransportDatabase.initDatabaseFromLocalDumpIfNecessary(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        FlowManager.destroy();
    }
}
