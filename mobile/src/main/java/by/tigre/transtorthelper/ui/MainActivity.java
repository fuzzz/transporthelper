package by.tigre.transtorthelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.raizlabs.android.dbflow.runtime.transaction.TransactionListenerAdapter;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import by.tigre.transtorthelper.R;
import by.tigre.transtorthelper.core.NotificationUtil;
import by.tigre.transtorthelper.core.data.entities.Stop;
import by.tigre.transtorthelper.core.data.entities.Stop$Table;
import by.tigre.transtorthelper.core.data.entities.Transport;
import by.tigre.transtorthelper.service.TransportLocationService;

import static by.tigre.transtorthelper.core.data.TransportDatabase.dLat;
import static by.tigre.transtorthelper.core.data.TransportDatabase.dLng;

public class MainActivity extends ActionBarActivity{
    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = new Intent(this, TransportLocationService.class);
        startService(i);
//
//        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        String mocLocationProvider = LocationManager.GPS_PROVIDER;
//        locationManager.addTestProvider(mocLocationProvider, false, false,
//                false, false, true, false, false, 0, 5);
//        locationManager.setTestProviderEnabled(mocLocationProvider, true);
//
//        MocGpsProvider p = new MocGpsProvider(this);
//        p.execute("");
    }

    void someDbHowtosLol() {
        // place/Хулиган/@53.8928573,27.5717192

        double testLat = 53.8928573;
        double testLon = 27.5717192;

        // Example of ASYNC query to DB. Android should work in non-ui-thread with db.
        new Select().from(Stop.class)
                .where(Condition.column(Stop$Table.LAT).between(testLat - dLat).and(testLat + dLat)
                        .and(Condition.column(Stop$Table.LNG).between(testLon - dLng).and(testLon + dLng)))
                .transactList(new TransactionListenerAdapter<List<Stop>>() {
                    @Override
                    public void onResultReceived(List<Stop> stops) {
                        Log.d(TAG, "\n\n\nNEARBY STOPS: \n\n\n");
                        for (Stop stop : stops) {
                            Log.d(TAG, stop.toString());
                        }
                    }
                });

        new Select().from(Stop.class)
                .where()
                .limit(10)
                .transactList(new TransactionListenerAdapter<List<Stop>>() {
                    @Override
                    public void onResultReceived(List<Stop> stops) {
                        Log.d(TAG, "\n\n\n10 STOPS: \n\n\n");
                        for (Stop stop : stops) {
                            Log.d(TAG, stop.toString());
                        }
                    }
                });

        // SYNC query sample
        List<Transport> transports = new Select().all().from(Transport.class).queryList();

        Log.d(TAG, "\n\n\nTRANSPORT: \n\n\n" + Arrays.toString(transports.toArray()));
    }

    public void testClick(View v) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("A87").append(" -> ").append("4 min").append("\n");
                stringBuilder.append("A33").append(" -> ").append("7 min").append("\n");
                stringBuilder.append("T12").append(" -> ").append("9 min").append("\n");
                NotificationUtil.sendExtandableNotification(stringBuilder.toString(), "Сцена EventSpace", MainActivity.this);
            }
        };
        timer.schedule(task, 3000); // ms
    }
}
