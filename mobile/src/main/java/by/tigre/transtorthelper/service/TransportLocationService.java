package by.tigre.transtorthelper.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.List;

import by.tigre.transtorthelper.core.GeofenceController;
import by.tigre.transtorthelper.core.data.TransportDatabase;
import by.tigre.transtorthelper.core.data.entities.Stop;

/**
 * Created by fz on 28.03.15.
 */
public class TransportLocationService extends Service
        implements  GoogleApiClient.ConnectionCallbacks,
                    GoogleApiClient.OnConnectionFailedListener,
                    LocationListener {

    public final static String TAG = "TrLocService";
    private final int UPDATE_INTERVAL_IN_MILLS = 3 * 60 * 1000;
    private final int FAST_UPDATE_INTERVAL_IN_MILLS = 2 * 60 * 1000;
    private final int IDLE_UPDATE_INTERVAL_IN_MILLS = 30 * 60 * 1000;

    GoogleApiClient locationClient;
    GeofenceController controlPoints;
    PendingResult<Status> ongoingRequest;

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "no, please, don't bind");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Created");
        initLocationUpdater();
    }

    private void initLocationUpdater() {
        locationClient  = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        controlPoints = new GeofenceController(locationClient, getApplicationContext());
        startListeningToLocationChanges();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //whatever
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "connecting to APIs failed");
    }

    @Override
    public void onDestroy() {
        stopListeningToLocationChanges();
        if(locationClient != null && locationClient.isConnected()) {
            locationClient.disconnect();
        }
        super.onDestroy();
    }

    private void startListeningToLocationChanges() {
        LocationRequest request = LocationRequest.create();
        request.setFastestInterval(UPDATE_INTERVAL_IN_MILLS);
        request.setPriority(LocationRequest.PRIORITY_LOW_POWER);

        //todo check if this is better
        //request.setInterval(IDLE_UPDATE_INTERVAL_IN_MILLS);
        //request.setFastestInterval(FAST_UPDATE_INTERVAL_IN_MILLS);
        //request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        ongoingRequest =
         LocationServices.FusedLocationApi.requestLocationUpdates(locationClient, request, this);

        Log.d(TAG, "Listening for location changes");
    }

    private void stopListeningToLocationChanges() {
        ongoingRequest.cancel();
        LocationServices.FusedLocationApi.removeLocationUpdates(locationClient, this);
        controlPoints.UpdateStops(new ArrayList<Stop>(1));
        Log.d(TAG, "Not listening for location changes anymore");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Got location: " + location.toString());
        List<Stop> stopsFromDb =
                TransportDatabase.getStopsInAreaAround(location);
        controlPoints.UpdateStops(stopsFromDb);
    }

}
