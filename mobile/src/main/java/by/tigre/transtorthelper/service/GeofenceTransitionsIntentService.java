package by.tigre.transtorthelper.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import by.tigre.transtorthelper.core.NotificationUtil;
import by.tigre.transtorthelper.core.data.TransportDatabase;

/**
 * IntentService to process Geofence transition events
 */
public class GeofenceTransitionsIntentService extends IntentService{
    private String NOTIFICATION_MOCK_TEXT = "This is a multiline test text\n This is a multiline test text\n This is a multiline test text\n This is a multiline test text\n This is a multiline test text";
    public final static String TAG = "FenceService";

    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceStatusCodes.getStatusCodeString(geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        //todo: somewhere here add somebody who knows better

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL /*>||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT*/) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            if(triggeringGeofences.size() > 0) {
                Log.e(TAG, "We're NEAR the stop! " + triggeringGeofences.get(0).getRequestId());
                // Get the transition details as a String.
                String text = prepareTransportForStopText(Long.parseLong(triggeringGeofences.get(0).getRequestId()));
                // Send notification and log the transition details.
                NotificationUtil.sendNotification(text, "Сцена EventSpace", this);
            }
        } else {
            // Log the error.
            Log.e(TAG, "lol, something is bad about geofence intents");
        }
    }

    private String prepareTransportForStopText(long stopId) {
        TransportDatabase.getRoutesAtTheStop(stopId);
        return "We're near " + stopId + " stop";
    }

}
