package by.tigre.transtorthelper.core.data.entities;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by igor on 3/28/15.
 *

 -- ----------------------------
 --  Table structure for route_stops
 -- ----------------------------
 DROP TABLE IF EXISTS "route_stops";
 CREATE TABLE "route_stops" ("route_id" INTEGER NOT NULL ,"stop_id" INTEGER NOT NULL ,"stop_index" INTEGER NOT NULL );

 */

@Table("route_stops")
public class RouteStop extends BaseModel {

    @Column(columnType = Column.PRIMARY_KEY_AUTO_INCREMENT)
    public int id;

    @Column(name = "route_id")
    public int routeId;

    @Column(name = "stop_id")
    public int stopId;

    @Column(name = "stop_index")
    public int stopIndex;

    @Override
    public String toString() {
        return "RouteStop{" + "id=" + id + ", routeId=" + routeId + ", stopId=" + stopId + ", stopIndex=" + stopIndex + '}';
    }
}
