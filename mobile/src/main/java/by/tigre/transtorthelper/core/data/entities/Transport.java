package by.tigre.transtorthelper.core.data.entities;

import android.support.annotation.IntDef;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ContainerAdapter;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by igor on 3/28/15.
 *
 *
 */

@Table("transport_type")
public class Transport extends BaseModel {

    @IntDef({BUS, METRO, TRAM, TROL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {}

    public static final int BUS = 1;
    public static final int METRO = 2;
    public static final int TRAM = 3;
    public static final int TROL = 4;

    @Column(columnType = Column.PRIMARY_KEY)
    public int id;

    @Column
    public int name;

    @Override
    public String toString() {
        return "Transport{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}


