package by.tigre.transtorthelper.core.utils;

/**
 * Created by fz on 29.03.15.
 */

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

/** Define a mock GPS provider as an asynchronous task of this Activity. */
public class MocGpsProvider extends AsyncTask<String, Integer, Void> {
    public static final String LOG_TAG = "GpsMockProvider";
    public static final String GPS_MOCK_PROVIDER = "GpsMockProvider";
    private Context ctx;

    ArrayList<String> data;

    public MocGpsProvider(Context c) {
        ctx = c;
        data = new ArrayList<>();

        for(int i = 0; i < 12; i++) {
            data.add("53.89098,27.5721");
        }


        for(int i = 0; i < 12; i++) {
            data.add("53.8923,27.5682");
        }



        for(int i = 0; i < 12; i++) {
            data.add("53.89236,27.56793");
        }


        for(int i = 0; i < 12; i++) {
            data.add("53.89085,27.57229");
        }

    }
    /**
     * Keeps track of the currently processed coordinate.
     */


    @Override
    protected Void doInBackground(String... data) {
        // process data
        for (String str : data) {
            // let UI Thread know which coordinate we are processing

            // retrieve data from the current line of text
            Double latitude = null;
            Double longitude = null;
            Double altitude = null;
            try {
                String[] parts = str.split(",");
                latitude = Double.valueOf(parts[0].trim());
                longitude = Double.valueOf(parts[1].trim());
                altitude = Double.valueOf(234);
            } catch (NullPointerException e) {
                Log.e(LOG_TAG, "NULL FuUUUUUUU");
                break;
            }        // no data available
            catch (Exception e){
                Log.e(LOG_TAG, "FuUUUUUUU " + e.toString() );
                continue;
            }                // empty or invalid line

            // translate to actual GPS location
            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            location.setAltitude(altitude);
            location.setTime(System.currentTimeMillis());

            // show debug message in log
            Log.d(LOG_TAG, location.toString());

            // provide the new location
            LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
            locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, location);
            Log.e(LOG_TAG, "posting location " + location );
            // sleep for a while before providing next location
            try {
                Thread.sleep(1000);

                // gracefully handle Thread interruption (important!)
                if (Thread.currentThread().isInterrupted())
                    throw new InterruptedException("");
            } catch (InterruptedException e) {
                break;
            }

        }

        return null;
    }
}