package by.tigre.transtorthelper.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import by.tigre.transtorthelper.R;
import by.tigre.transtorthelper.ui.MainActivity;

public class NotificationUtil {
    private final static String TAG = "NotificationUtil";
    private final static String NOTIFICATION_MOCK_TEXT = "This is a multiline test text\n This is a multiline test text\n This is a multiline test text\n This is a multiline test text\n This is a multiline test text";

    public static void sendNotification(String message, String title, Context context) {
            Log.d(TAG, String.format("In Notification Builder. Message: %s ; Context: %s", message, context));
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.bus)
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setContentTitle(title)
                            .setContentText(message);
            NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);
            mNotificationManager.notify(1, mBuilder.build());
        }

    public static void sendExtandableNotification(String message, String title, Context context) {
        Log.d(TAG, String.format("In Notification Builder. Message: %s ; Context: %s", message, context));
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_bus_stop_symbol)
                        .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.bus)).getBitmap())
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentText(message)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setVibrate(new long[]{ 200, 200, 200});
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }

    public static String getMockMessage() {
        return NOTIFICATION_MOCK_TEXT;
    }
}
