package by.tigre.transtorthelper.core.data.entities;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by igor on 3/28/15.
 *
 *
 -- ----------------------------
 --  Table structure for routes
 -- ----------------------------
 DROP TABLE IF EXISTS "routes";
 CREATE TABLE "routes" (
 "id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE ,
 "route_num" VARCHAR NOT NULL ,
 "transport_type_id" INTEGER NOT NULL ,
 "operator_id" INTEGER,
 "route_type_id" INTEGER,
 "name" VARCHAR NOT NULL ,
 "week_days" INTEGER,
 "week_days_details" VARCHAR,
 "start_time" VARCHAR,
 "deltas" VARCHAR,
 "number_of_stops" INTEGER,
 "route_num_int" INTEGER,
 "route_stops" VARCHAR);
 */

@Table("routes")
public class Route extends BaseModel {

    @Column(columnType = Column.PRIMARY_KEY, notNull = true)
    public int id;

    @Column(name = "route_num")
    public String routeNumber;

    @Column(name = "transport_type_id")
    public int transportType;

    @Column(name = "operator")
    public int operator;

    @Column(name = "route_type_id")
    public int routeType;

    @Column
    public String name;

    @Column(name = "week_days")
    public int weekDays;

    @Column(name = "week_days_details")
    public int weekDaysDetails;

    @Column(name = "start_time")
    public String startTime;

    @Column(name = "deltas")
    public int deltas;

    @Column(name = "number_of_stops")
    public int numberOfStops;

    @Column(name = "route_num_int")
    public int routeNumberInt;

    @Column(name = "route_stops")
    public String routeStops;


}
