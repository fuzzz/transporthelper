package by.tigre.transtorthelper.core.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import by.tigre.transtorthelper.core.data.entities.Route;
import by.tigre.transtorthelper.core.data.entities.Route$Table;
import by.tigre.transtorthelper.core.data.entities.Stop;
import by.tigre.transtorthelper.core.data.entities.Stop$Table;

/**
 * Created by igor on 3/28/15.
 */

@Database(name = TransportDatabase.DATABASE_NAME, version = TransportDatabase.VERSION, foreignKeysSupported = true)
public class TransportDatabase {
    public static final int VERSION = 1;
    public static final String DATABASE_NAME = "transport";
    public static final String KEY_IS_DB_READY = TransportDatabase.class.getPackage() + ".DB_READY";
    public static final String DUMP_FILE_PATH = "db/transport.db";
    public static final String TAG = TransportDatabase.class.getName();

    public static final double dLat = 0.0045d; //~0.5km
    public static final double dLng = 0.0076284d; // ~0.5km 53

    public static void initDatabaseFromLocalDumpIfNecessary(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!preferences.getBoolean(KEY_IS_DB_READY, false)) {
            // workaround to create DB file before replacing it
            FlowManager.getDatabase(TransportDatabase.DATABASE_NAME).getWritableDatabase();
            InputStream localDbInput;
            try {
                localDbInput = context.getAssets().open(DUMP_FILE_PATH);
                copyInputStreamToFile(localDbInput, context.getDatabasePath(DATABASE_NAME + ".db"));
                Log.d(TAG, "Database imported successfully. Great news!");
                preferences.edit().putBoolean(KEY_IS_DB_READY, true).apply();
            } catch (IOException e) {
                Log.e(TAG, "Error while copying database!", e);
            }
        }
    }

    public static void copyInputStreamToFile(InputStream in, File file) throws IOException {
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();
    }

    public static List<Stop> getStopsInAreaAround(Location l) {
        return new Select().from(Stop.class)
                .where(Condition.column(Stop$Table.LAT).between(l.getLatitude() - dLat / 2).and(l.getLatitude() + dLat / 2))
                .and(Condition.column(Stop$Table.LNG).between(l.getLongitude() - dLng / 2).and(l.getLongitude() + dLng / 2))
                .queryList();
    }

    public static List<Route> getRoutesAtTheStop(long stopId) {
        return new Select().from(Route.class).queryList();
    }
}


