package by.tigre.transtorthelper.core.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class OldDownLoaderHelper {
    protected final String link;
    protected final String file;
    protected String date;
    protected final int type;

    private static final int CONNECTION_TIMEOUT = 3*60 * 1000;

	public OldDownLoaderHelper(String link, String file, String date, int type) {
		super();
        Log.i("UpdateService", "file = " + link);
		this.link = link;
		this.file = file;
		this.date = date;
        this.type = type;
    }

	private String error;

	public static boolean downloadHttpTr(Context context, String link, String dbPath) {
		boolean result = true;

		Log.i("UpdateService", "down start = DB");
        URLConnection mConnection;
        try {
            mConnection = (URLConnection) new URL(link).openConnection();
        } catch (IOException e) {
            Log.e("UpdateService", "URL is malformed or unable to connect");
            return false;
        }
        mConnection.setConnectTimeout(CONNECTION_TIMEOUT);

        InputStream data = null;
        File file = new File(dbPath);
        try {
            data = mConnection.getInputStream();
            file.mkdirs();
            if (!ZIP.UnPackZIP(data, file.getAbsolutePath())) {
                Log.e("UpdateService", "unable to unzip");
            }
        } catch (IOException e) {
            Log.e("UpdateService", "unable to read data");
            result = false;
        } finally {
            try {
                data.close();
            } catch (IOException e) {
                Log.e("UpdateService", "stream close failed");
            }
        }
		Log.i("UpdateService", "down finish DB");
		return result;
	}

    /*
    FIXME закомментил это, потом поудаляем это все к чертям. Igor.
    public static boolean loadDbFromAssets(Context context) {
        Log.i("UpdateService", "down start = DB");
        boolean result;
        InputStream data;

        File databaseDestinationFile = new File(Utils.getDBFolderPath(context), DataBaseHelper.DB_NAME);
        try {
            data = context.getAssets().open("db/transport.db");
            copyInputStreamToFile(data, databaseDestinationFile);
            result = true;
        } catch (IOException e) {
            Log.e("UpdateService", "unable to read data");
            result = false;
        }
        Log.i("UpdateService", "down finish DB");
        return result;
    }*/

    public static void copyInputStreamToFile( InputStream in, File file ) throws IOException {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
    }
}
