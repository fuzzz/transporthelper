package by.tigre.transtorthelper.core.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

public class StopT extends BaseElement implements Parcelable {
    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public String getWeekdaysDetals() {
        return weekdaysDetals;
    }

    public void setWeekdaysDetals(String weekdaysDetals) {
        this.weekdaysDetals = weekdaysDetals;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDeltas() {
        return deltas;
    }

    public void setDeltas(String deltas) {
        this.deltas = deltas;
    }

    public String getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(String weekdays) {
        this.weekdays = weekdays;
    }

    public final int id;
    private String startTime;
    private String deltas;
    private String weekdays;
    private String weekdaysDetals;
    private int iterations;
    private int numberStopsInRout;

    public StopT(int id, String name, double lan, double lng) {
        super(Html.fromHtml(name).toString(), lan, lng);
        this.id = id;
    }

    @Override
    public String toString() {
        return "StopT{" +
                "id=" + id +
                ", startTime='" + startTime + '\'' +
                ", deltas='" + deltas + '\'' +
                ", weekdays='" + weekdays + '\'' +
                ", weekdaysDetals='" + weekdaysDetals + '\'' +
                ", iterations=" + iterations +
                ", numberStopsInRout=" + numberStopsInRout +
                "} " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.startTime);
        dest.writeString(this.deltas);
        dest.writeString(this.weekdays);
        dest.writeString(this.weekdaysDetals);
        dest.writeInt(this.iterations);
        dest.writeInt(this.numberStopsInRout);
        dest.writeString(this.name);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    private StopT(Parcel in) {
        this.id = in.readInt();
        this.startTime = in.readString();
        this.deltas = in.readString();
        this.weekdays = in.readString();
        this.weekdaysDetals = in.readString();
        this.iterations = in.readInt();
        this.numberStopsInRout = in.readInt();
        this.name = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Parcelable.Creator<StopT> CREATOR = new Parcelable.Creator<StopT>() {
        public StopT createFromParcel(Parcel source) {
            return new StopT(source);
        }

        public StopT[] newArray(int size) {
            return new StopT[size];
        }
    };
}
