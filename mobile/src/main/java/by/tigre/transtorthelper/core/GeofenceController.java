package by.tigre.transtorthelper.core;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.LongSparseArray;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import by.tigre.transtorthelper.core.data.entities.Stop;
import by.tigre.transtorthelper.service.GeofenceTransitionsIntentService;

/**
 * Created by fz on 28.03.15.
 */
public class GeofenceController implements ResultCallback<Status> {
    public final static String TAG = "GeofenceController";
    private static final float GEOFENCE_RADIUS_IN_METERS = 30f;
    private static final int LOITERING_DELAY = 10 * 1000 ;
    private static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = TimeUnit.MINUTES.toMillis(60);

    LongSparseArray<PendingIntent> fencedStops = new LongSparseArray<>();

    Context context;
    GoogleApiClient geofenceClient;

    public GeofenceController(GoogleApiClient fenceClient, Context c) {
        geofenceClient = fenceClient;
        context = c;
    }

    private void addTransportStop(long stopId, double lat, double lon ) {
        Log.d(TAG, "adding stop " + stopId + " near " + lat +" " + lon);
        Geofence g = createGeofenceForStop("" + stopId, lat, lon);
        PendingIntent intentForFence = getGeofencePendingIntent();
        //register intents for fencing
        LocationServices.GeofencingApi.addGeofences(
                geofenceClient,
                createGeofencingRequest(g),
                intentForFence).setResultCallback(this);
        fencedStops.put(stopId, intentForFence);
    }

    private void removeTransportStop(long stopId) {
        Log.d(TAG, "removing stop " + stopId);
        if(fencedStops.indexOfKey(stopId) < 0) {
            return;
        }
        //unregister intents for fencing
        LocationServices.GeofencingApi.
                removeGeofences(geofenceClient, fencedStops.get(stopId))
                    .setResultCallback(this);
        fencedStops.remove(stopId);
    }

    private Geofence createGeofenceForStop(String stopName, double latitude, double longitude) {
        return new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this geofence.
                .setRequestId(stopName)
                .setCircularRegion(latitude, longitude, GEOFENCE_RADIUS_IN_METERS)
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setLoiteringDelay(LOITERING_DELAY)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL)
                .build();
    }

    public void UpdateStops(List<Stop> stops) {
        //removing fences that are not around anymore
        int idx = 0;
        while (idx < fencedStops.size());
        {
            boolean shouldBeRemoved = true;
            for(Stop newStop : stops) {
                if(newStop.id == fencedStops.keyAt(idx)) {
                    shouldBeRemoved = false;
                    break;
                }
            }

            if(shouldBeRemoved) {
                fencedStops.removeAt(idx);
            }
            else {
                idx++;
            }
        }
        //adding new
        for (Stop stop : stops) {
            if(fencedStops.indexOfKey(stop.id) < 0) {
                addTransportStop(stop.id , stop.lat, stop.lng);
            }
        }
    }

    private GeofencingRequest createGeofencingRequest(Geofence f) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL);
        builder.addGeofence(f);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onResult(Status status) {
        //todo: possibly need to remove this
        //geofence add/remove status
        Log.d(TAG, "fence creation/destruction success? " + status.isSuccess());
    }
}
