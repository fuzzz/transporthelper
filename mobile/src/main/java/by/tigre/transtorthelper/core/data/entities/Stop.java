package by.tigre.transtorthelper.core.data.entities;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.Model;

/**
 * Created by igor on 3/28/15.
 *
 *
 */

@Table("stops")
public final class Stop extends BaseModel {

    @Column(columnType = Column.PRIMARY_KEY, notNull = true)
    public long id;

    @Column
    public String street;

    @Column
    public String name;

    @Column
    public double lat;

    @Column
    public double lng;

    @Column(name = "ttype_on_stop")
    public int typeOnStop;

    @Column(name = "close_stops")
    public String closeStops;

    @Override
    public String toString() {
        return "Stop{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", typeOnStop=" + typeOnStop +
                ", closeStops='" + closeStops + '\'' +
                '}';
    }
}
