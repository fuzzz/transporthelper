/*
 Navicat Premium Data Transfer

 Source Server         : Transport
 Source Server Type    : SQLite
 Source Server Version : 3008000
 Source Database       : main

 Target Server Type    : SQLite
 Target Server Version : 3008000
 File Encoding         : utf-8

 Date: 03/28/2015 23:31:30 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for route_stops
-- ----------------------------
DROP TABLE IF EXISTS "route_stops";
CREATE TABLE "route_stops" ("route_id" INTEGER NOT NULL ,"stop_id" INTEGER NOT NULL ,"stop_index" INTEGER NOT NULL );

PRAGMA foreign_keys = true;
