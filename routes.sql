/*
 Navicat Premium Data Transfer

 Source Server         : Transport
 Source Server Type    : SQLite
 Source Server Version : 3008000
 Source Database       : main

 Target Server Type    : SQLite
 Target Server Version : 3008000
 File Encoding         : utf-8

 Date: 03/28/2015 23:18:15 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for routes
-- ----------------------------
DROP TABLE IF EXISTS "routes";
CREATE TABLE "routes" (
"id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE ,
"route_num" VARCHAR NOT NULL ,
"transport_type_id" INTEGER NOT NULL ,
"operator_id" INTEGER,
"route_type_id" INTEGER,
"name" VARCHAR NOT NULL ,
"week_days" INTEGER,
"week_days_details" VARCHAR,
"start_time" VARCHAR,
"deltas" VARCHAR,
"number_of_stops" INTEGER,
"route_num_int" INTEGER,
"route_stops" VARCHAR);

PRAGMA foreign_keys = true;
